#!/bin/bash

# Install UFW
sudo apt-get --assume-yes update
sudo apt-get --assume-yes install ufw

sudo ufw allow ssh
# DNS servers
sudo ufw allow 53/udp # quivalent  UDP/IPv4 & UDP/IPv6 pour le port 53 dans L3
sudo ufw allow from 94.237.40.9 to any port 53
sudo ufw allow from 94.237.127.9 to any port 53
sudo ufw allow from 2a04:3544:53::1 to any port 53
sudo ufw allow from 2a04:3540:53::1 to any port 53
# ubuntu-1cpu-1gb-us-chi1
sudo ufw allow from 10.3.8.39 to any port 6379
sudo ufw allow from 10.3.8.39 to any port 26379
sudo ufw allow from 10.3.8.39 to any port 5555
sudo ufw allow from 10.3.8.39 to any port 5556
sudo ufw allow from 10.3.8.39 to any port 5560
sudo ufw allow from 10.3.8.39 to any port 1099
# ubuntu-1cpu-1gb-us-sjo1
sudo ufw allow from 10.8.0.83 to any port 6379
sudo ufw allow from 10.8.0.83 to any port 26379
sudo ufw allow from 10.8.0.83 to any port 5555
sudo ufw allow from 10.8.0.83 to any port 5556
sudo ufw allow from 10.8.0.83 to any port 5560
sudo ufw allow from 10.8.0.83 to any port 1099
# ubuntu-1cpu-1gb-us-lon1
sudo ufw allow from 10.2.6.167 to any port 6379
sudo ufw allow from 10.2.6.167 to any port 26379
sudo ufw allow from 10.2.6.167 to any port 5555
sudo ufw allow from 10.2.6.167 to any port 5556
sudo ufw allow from 10.2.6.167 to any port 5560
sudo ufw allow from 10.2.6.167 to any port 1099
sudo ufw --force enable

# Pour afficher le status du firewall
# sudo ufw status
# sudo ufw status verbose
