#!/bin/bash

# Redis configuration
REDIS_MASTER_IP_ADDRESS="127.0.0.1" # Note: use private ip address
REDIS_IS_SLAVE=0

# MySQL configuration
MYSQL_SERVER_ID=1
AUTO_INCREMENT_OFFSET=1

# Prepare required package for installation
sudo apt-get --assume-yes update 
sudo apt-get --assume-yes install build-essential tcl
sudo apt-get --assume-yes install libjemalloc-dev

# Install latest stable Redis
curl -O http://download.redis.io/redis-stable.tar.gz
tar xzvf redis-stable.tar.gz
cd redis-stable
make
sudo make install
cd /

#### Configure Redis
sudo mkdir /etc/redis
sudo cp /redis-stable/redis.conf /etc/redis
sudo chmod 666 /etc/redis/redis.conf
sed -i -e 's/supervised no/supervised systemd/g' /etc/redis/redis.conf
sed -i -e 's/dir .\//dir \/var\/lib\/redis/g' /etc/redis/redis.conf
sed -i -e 's/logfile ""/logfile \/var\/log\/redis.log/g' /etc/redis/redis.conf
sed -i -e 's/protected-mode yes/protected-mode no/g' /etc/redis/redis.conf
sed -i -e 's/# requirepass foobared/requirepass A8P5jeG52mp2bhEwESpWa9Zt/g' /etc/redis/redis.conf
sed -i -e 's/# masterauth <master-password>/masterauth A8P5jeG52mp2bhEwESpWa9Zt/g' /etc/redis/redis.conf
if [ $REDIS_IS_SLAVE == 1 ]; then
	echo "slaveof $REDIS_MASTER_IP_ADDRESS 6379" >> /etc/redis/redis.conf
fi

# Configure Redis as service
printf '[Unit]
Description=Redis In-Memory Data Store
After=network.target\n
[Service]
User=redis
Group=redis
ExecStart=/usr/local/bin/redis-server /etc/redis/redis.conf
ExecStop=/usr/local/bin/redis-cli shutdown
Restart=always\n
[Install]
WantedBy=multi-user.target\n' > /etc/systemd/system/redis.service

# Create user redis
sudo adduser --system --group --no-create-home redis

# Create Redis DB working directory
sudo mkdir /var/lib/redis
sudo chown redis:redis /var/lib/redis
sudo chmod 770 /var/lib/redis

# Create Redis log file
printf '' > /var/log/redis.log
sudo chown redis:redis /var/log/redis.log

#### Configure Sentinel
sudo cp /redis-stable/sentinel.conf /etc/redis/
sudo chmod 666 /etc/redis/sentinel.conf
sed -i -e 's/# protected-mode no/protected-mode no/g' /etc/redis/sentinel.conf
sed -i -e 's/logfile ""/logfile \/var\/log\/sentinel.log/g' /etc/redis/sentinel.conf
sed -i -e 's/dir \/tmp/dir \/var\/lib\/redis\/sentinel/g' /etc/redis/sentinel.conf
sed -i -e "s/sentinel monitor mymaster 127.0.0.1 6379 2/sentinel monitor mymaster $REDIS_MASTER_IP_ADDRESS 6379 2/g" /etc/redis/sentinel.conf
sed -i -e 's/sentinel down-after-milliseconds mymaster 30000/sentinel down-after-milliseconds mymaster 5000/g' /etc/redis/sentinel.conf
sed -i -e 's/sentinel failover-timeout mymaster 180000/sentinel failover-timeout mymaster 10000/g' /etc/redis/sentinel.conf
sed -i -e 's/# sentinel auth-pass mymaster MySUPER--secret-0123passw0rd/sentinel auth-pass mymaster A8P5jeG52mp2bhEwESpWa9Zt/g' /etc/redis/sentinel.conf

# Configure Sentinel as service
printf '[Unit]
Description=Sentinel for Redis
After=network.target\n
[Service]
User=redis
Group=redis
ExecStart=/usr/local/bin/redis-sentinel /etc/redis/sentinel.conf
ExecStop=/usr/local/bin/redis-cli shutdown
Restart=always\n
[Install]
WantedBy=multi-user.target\n' > /etc/systemd/system/sentinel.service

# Create Sentinel working directory
sudo mkdir /var/lib/redis/sentinel
sudo chown redis:redis /var/lib/redis/sentinel
sudo chmod 770 /var/lib/redis/sentinel

# Create Sentinel log file
printf '' > /var/log/sentinel.log
sudo chown redis:redis /var/log/sentinel.log

# Production performance optimizaton
sed -i -e 's/# By default this script does nothing./sysctl -w net.core.somaxconn=65535\necho never > \/sys\/kernel\/mm\/transparent_hugepage\/enabled/g' /etc/rc.local
echo "vm.overcommit_memory = 1" >> /etc/sysctl.conf

# Start Redis & Sentinel
sudo systemctl start redis
sudo systemctl start sentinel

# Enable Redis & Sentinel to Start at Boot
sudo systemctl enable redis
sudo systemctl enable sentinel

############################################################################################################################################
# TODO: Pour Redis master
# NOTE: une fois le serveur cr et dploy, il faut aller remplacer l'adresse IP par l'adresse priv du serveur dans /etc/redis/sentinel.conf
# sentinel monitor mymaster 127.0.0.1 6379 2	--->  sentinel monitor mymaster {PRIVATE IP ADDRESS} 6379 2
# Il faut galement aller ajouter cette adresse dans la fin de la proprit "bind 127.0.0.1" dans /etc/redis/redis.conf
# ex.: bind 127.0.0.1 10.3.8.39
############################################################################################################################################

# Install UFW
sudo apt-get --assume-yes update
sudo apt-get --assume-yes install ufw

sudo ufw allow ssh
# DNS servers
sudo ufw allow 53/udp
sudo ufw allow from 94.237.40.9 to any port 53
sudo ufw allow from 94.237.127.9 to any port 53
sudo ufw allow from 2a04:3544:53::1 to any port 53
sudo ufw allow from 2a04:3540:53::1 to any port 53

#### TODO: mettre le serveur que l'on cr actuellement en commentaire
# ubuntu-1cpu-1gb-us-chi1
sudo ufw allow from 10.3.8.39 to any port 6379
sudo ufw allow from 10.3.8.39 to any port 26379
# ubuntu-1cpu-1gb-us-sjo1
sudo ufw allow from 10.8.0.83 to any port 6379
sudo ufw allow from 10.8.0.83 to any port 26379
# ubuntu-1cpu-1gb-uk-lon1
sudo ufw allow from 10.2.6.167 to any port 6379
sudo ufw allow from 10.2.6.167 to any port 26379
sudo ufw --force enable

# Install Java with SDKMan
sudo apt-get --assume-yes install zip
cd /root
curl -s "https://get.sdkman.io" | bash
source "/root/.sdkman/bin/sdkman-init.sh"
#sdk install java 8.0.212-zulu
sdk install java 11.0.6-zulu

# Install Redis-stat
sudo mkdir /tools
cd /tools
wget https://gitlab.com/dpageau/environment-public/raw/master/tools/redis-stat-0.4.14.jar
sudo ufw allow 63790
# To run Redis-stat
#java -jar /tools/redis-stat-0.4.14.jar --auth=A8P5jeG52mp2bhEwESpWa9Zt --server

# Install MySQL (https://www.linode.com/docs/databases/mysql/configure-master-master-mysql-database-replication/)
sudo apt-get --assume-yes update
sudo apt-get --assume-yes upgrade -y
sudo apt-get --assume-yes install mysql-server mysql-client

# Configure security (manually)
#sudo mysql_secure_installation

sudo cp /etc/mysql/mysql.conf.d/mysqld.cnf /etc/mysql/mysql.conf.d/mysqld.cnf.backup
########################################################################################################################################################
# TODO: Pour MySQL
# NOTE: une fois le serveur cr et dploy, il faut aller remplacer l'adresse IP par l'adresse priv du serveur dans /etc/mysql/mysql.conf.d/mysqld.cnf
# bind-address            = 127.0.0.1     ---->>   bind-address            = {PRIVATE IP}
# Il faut galement ajouter l'entre suivante en dessous de bind-address (This will disable loading files from the filesystem for users without file level privileges to the database) 
# local-infile=0
########################################################################################################################################################
sed -i -e 's/#server-id              = 1/server-id              = $MYSQL_SERVER_ID/g' /etc/mysql/mysql.conf.d/mysqld.cnf
sed -i -e 's/#log_bin                        = /var/log/mysql/mysql-bin.log/log_bin                        = /var/log/mysql/mysql-bin.log\nlog_bin_index       = /var/log/mysql/mysql-bin.log.index\n
relay_log           = /var/log/mysql/mysql-relay-bin\nrelay_log_index     = /var/log/mysql/mysql-relay-bin.index/g' /etc/mysql/mysql.conf.d/mysqld.cnf
sed -i -e 's/max_binlog_size   = 100M/max_binlog_size   = 100M\nlog_slave_updates   = 1\nauto-increment-increment = 2\nauto-increment-offset = $AUTO_INCREMENT_OFFSET/g' /etc/mysql/mysql.conf.d/mysqld.cnf
# Restart MySQL
sudo systemctl restart mysql

# Chicago must authorize San Jose
sudo ufw allow from 10.8.0.83 to any port 3306
# San Jose must authorize Chicago
sudo ufw allow from 10.3.8.39 to any port 3306
########################################################################################################################################################
# Need to configure L3 too by opening same ports
########################################################################################################################################################

# Create Replication Users (manually) -> https://www.linode.com/docs/databases/mysql/configure-master-master-mysql-database-replication/#create-replication-users
# Configure Database Replication (manually) -> https://www.linode.com/docs/databases/mysql/configure-master-master-mysql-database-replication/#configure-database-replication
