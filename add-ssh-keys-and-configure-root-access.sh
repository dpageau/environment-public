#!/bin/bash

# Add SSH keys for current user
mkdir -p ~/.ssh
chmod 700 ~/.ssh
printf 'ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAQEAoYP6JpPl/qLf5+icLROMxvZot0XQYBuA/lFUooxpAwBHN02gKQbVsXDMqKCv6+qsaPd182duVnrunWwmyHZXNMoY+uzIKbe9fnVFWNRDQFOtyMZp46UFlnP1H25OAhdz62jSN5MSapjobKiiy9EJOPEW8ZR484+LmQ8eSKWnZtSbhhclUBOi+iXn+AWSgWnlWGh8EQyOHNlJRoIIVOqE6Ce9E6tN1/iyCSBdjA6pvAZRlnt41O+eABLoEruJNHVCzxF8PyHtj3Tgj7SZ8e+2dX+kc/EnPSCBjtlz+kAclxiY+p63OdRW6+yBZ33nyF/TnOT0DJKLmAqCons8mGaVHQ== rsa-key-20190202
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCfPC6lj5rJDuU9qv6E0U2vSQKK7MprKaKUnmH4iTt2pkSobfIXSzWqLfy4F+kw7o6XUvA8ovU7lUfmquZbc57naablCekgimC9Xns9uxga7k1ZowKkYBt1M7CApdBuRqtjy65NIjuXReEBKhiYEvnWHChuccBFZquEAv4Jpnfdg5Gz9oCogCnc1YosnKZMq2G5i9wy0kl7za/ATihPV/BM9UJGkwOMrnzOJWRLzHuAuZfPuxpDQL8hnUy6l8qEl7FjD45u3aD6xF4mg6rSHYvezvEK7emMTWDSYeX6EV2KmhoHc4rkQRc2PPXUtoBOVgFpl6+Wys3KbvZJUPWUXoORNrqUydgsCTwYyW9SoLKWeSLJzZUg8Oosdnocq/8hjobj9CzQ0zBSAqhfBX8RQmZ7AhMqQddVxy84T3FL6VcqunjqQJOB8hpT34O1ozXbWaEsm/c+AfkTSx76aVophVb+a5OAz+KSC1NI9sv+lJe87YenU0X7/VNdu8y1OePwPumHjfK0cVQmARBYvhFdyBZXBCCv9dKeLBpASCTp1A1IZea8b16fwTxNFPW5KeX630dwyI1JDhY5hyc/yCHThqZVfxahGJp6X+uSIdI3iheVn7SwxOxoIf0QsOodcJVMyPT7hcWea/FJn1ohpCoiof/K0ZZD3yjKyHDKNmtde2yRiQ== danny.pageau@ticketmaster.com\n' > ~/.ssh/authorized_keys

# Disable root login and password authentication
# source: https://upcloud.com/community/tutorials/use-ssh-keys-authentication/
sudo sed -i -e 's/PermitRootLogin yes/PermitRootLogin no/g' /etc/ssh/sshd_config
sudo sed -i -e 's/#PasswordAuthentication yes/PasswordAuthentication no/g' /etc/ssh/sshd_config
sudo sed -i -e 's/#PubkeyAuthentication yes/PubkeyAuthentication yes/g' /etc/ssh/sshd_config

sudo service ssh restart
sudo systemctl restart sshd
