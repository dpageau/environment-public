#!/bin/bash

sudo mkdir /home/dpageau/apps/spring-boot-admin
cd /home/dpageau/apps/spring-boot-admin
wget https://gitlab.com/dpageau/environment-public/raw/master/apps/spring-boot-admin/spring-boot-admin-1.0.0.0-SNAPSHOT.jar
wget https://gitlab.com/dpageau/environment-public/raw/master/configs/applications/spring-boot-admin/logback-prod.xml
sudo mkdir /var/log/spring-boot-admin
sudo chown dpageau /var/log/spring-boot-admin/
sudo chgrp dpageau /var/log/spring-boot-admin/
sudo ufw allow 8090

# Create bash script
printf '#!/bin/bash
source "/home/dpageau/.sdkman/bin/sdkman-init.sh"
java -jar -Dlogging.config=/home/dpageau/apps/spring-boot-admin/logback-prod.xml -Dspring.profiles.active=prod spring-boot-admin-1.0.0.0-SNAPSHOT.jar\n' > /home/dpageau/apps/spring-boot-admin/spring-boot-admin.sh

# Configure spring-boot-admin as a service
printf '[Unit]
Description=spring-boot-admin application\n
[Service]
User=dpageau
Group=dpageau
WorkingDirectory=/home/dpageau/apps/spring-boot-admin
ExecStart=/home/dpageau/apps/spring-boot-admin/spring-boot-admin.sh
SuccessExitStatus=143
TimeoutStopSec=10
Restart=on-failure
RestartSec=5\n
[Install]
WantedBy=multi-user.target\n' | sudo tee /etc/systemd/system/spring-boot-admin.service

sudo systemctl daemon-reload
sudo systemctl enable spring-boot-admin.service
sudo systemctl start spring-boot-admin.service

cd /

